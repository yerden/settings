syntax on

hi Search ctermbg=Yellow
hi Search ctermfg=Black
" Настроим кол-во символов пробелов, которые будут заменять \t
set tabstop=4
set shiftwidth=4
set smarttab
" set et " — включим автозамену по умолчанию
" set expandtab
set copyindent
" set smartindent
set preserveindent
set softtabstop=0

" case sensitive search
set noic
" nmap <F9> :set ignorecase! ignorecase?
autocmd FileType go nmap <F7> :GoTest<CR>
autocmd FileType go nmap <F9> :GoBuild<CR>
autocmd FileType go nmap <F10> :GoDef<CR>

set wrap " — попросим Vim переносить длинные строки

set ai " — включим автоотступы для новых строк
set cin " — включим отступы в стиле Си

" Далее настроим поиск и подсветку результатов поиска и совпадения скобок
set showmatch
set hlsearch
set incsearch

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

set lz " — ленивая перерисовка экрана при выполнении скриптов

" Показываем табы в начале строки точками
set listchars=tab:··,trail:·
set list

"Порядок применения кодировок и формата файлов

set ffs=unix,dos,mac
set fencs=utf-8,cp1251,koi8-r,ucs-2,cp866

"Взаимодействие и элементы интерфейса

"Я часто выделяю мышкой содержимое экрана в Putty, но перехват мышки в Vim мне иногда мешает. Отключаем функционал вне графического режима:
if !has('gui_running')
set mouse=
endif

"Избавляемся от меню и тулбара:
set guioptions-=T
set guioptions-=m

"В разных графических системах используем разные шрифты:
if has('gui')
colorscheme darkblue
if has('win32')
set guifont=Lucida_Console:h12:cRUSSIAN::
else
set guifont=Terminus\ 14
endif
endif

"Пытаемся занять максимально большое пространство на экране. Как водится, по-разному на разных системах:
if has('gui')
if has('win32')
au GUIEnter * call libcallnr('maximize', 'Maximize', 1)
elseif has('gui_gtk2')
au GUIEnter * :set lines=99999 columns=99999
endif
endif

"Опять же, системы сборки для разных платформ могут быть переопределены:
if has('win32')
set makeprg=nmake
compiler msvc
else
set makeprg=make
compiler gcc
endif

highlight Comment       ctermfg=DarkGrey
set laststatus=2
set t_Co=256

call plug#begin('~/.vim/plugged')

if has('nvim')
  "Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  "Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1
"Plug 'powerline/powerline',{'rtp':'powerline/bindings/vim/'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
filetype plugin on
"Plug 'mdempsky/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'fatih/vim-go',{'do':':GoUpdateBinaries'}
Plug 'majutsushi/tagbar'
Plug 'scrooloose/nerdcommenter'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'rust-lang/rust.vim'
Plug 'dense-analysis/ale'
Plug 'airblade/vim-rooter'

"set omnifunc=ale#completion#OmniFunc
let rustfmt_autosave=1

nmap <F8> :TagbarToggle<CR>
let g:go_def_mapping_enabled=0

call plug#end()
" For use with gotags
let g:tagbar_type_go = {
	\ 'ctagstype' : 'go',
	\ 'kinds'     : [
		\ 'p:package',
		\ 'i:imports:1',
		\ 'c:constants',
		\ 'v:variables',
		\ 't:types',
		\ 'n:interfaces',
		\ 'w:fields',
		\ 'e:embedded',
		\ 'm:methods',
		\ 'r:constructor',
		\ 'f:functions'
	\ ],
	\ 'sro' : '.',
	\ 'kind2scope' : {
		\ 't' : 'ctype',
		\ 'n' : 'ntype'
	\ },
	\ 'scope2kind' : {
		\ 'ctype' : 't',
		\ 'ntype' : 'n'
	\ },
	\ 'ctagsbin'  : 'gotags',
	\ 'ctagsargs' : '-sort -silent'
\ }

set bg=dark


" Disable arrow keys - force to use home line
nnoremap <up> <nop>
nnoremap <down> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>


let g:rooter_patterns = ['.vimroot', '.git/', '.python-version']

augroup vimrc_rooter
	autocmd VimEnter * :Rooter
augroup END
inoremap <right> <nop>
